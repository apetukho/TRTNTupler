# TRTNTupler

A package that uses TRTFramework to create ntuples.

Packages:
- TRTFramework (a git submodule): the framework/API providing a ncie
  environment for TRT analyses.
- MCDNTupler: The "main" package, has algorithm for Data and MC
  processing and base ntuple structure class.
  
# Installation 

```
mkdir TRTNtupler
setupATLAS
asetup 21.2.100,AnalysisBase
lsetup git
git clone --recursive https://:@gitlab.cern.ch:8443/apetukho/TRTNTupler.git
mkdir run build 
cd build
cmake ../TRTNTupler
make -j10
source x86_64-centos7-gcc62-opt/setup.sh
```

Running locally
```
runNTuplerAlg -c ../TRTNTupler/MCDNTupler/data/main.cfg --in-file local_file_paths_list.txt --outDS output_dataset_name -o output_folder_name
```
Running on grid
```
runNTuplerAlg -c ../TRTNTupler/MCDNTupler/data/main.cfg --gridDS grid_dataset_name --outDS output_dataset_name -o output_folder_name
```
# Configs

`MCDNTupler/data/main.cfg` - selection used for the [ATL-COM-INDET-2021-021](https://cds.cern.ch/record/2783151), 2021 RNN PID note by Aleksandr Petukhov

`MCDNTupler/data/mainOld.cfg` - selection used for the [ATL-COM-INDET-2020-001](ATL-COM-INDET-2020-001), 2020 RNN PID note by Arif Bayrili

# Other notes
Paths to custom GRLs should contain commas ',' at the end for the parser to work correctly, e.g.
`GRLFiles: GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml,`