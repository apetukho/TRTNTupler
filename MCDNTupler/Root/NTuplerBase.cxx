// ATLAS
#include <EventLoop/StatusCode.h>
#include <AsgTools/MessageCheck.h>

// ROOT
#include <TSystem.h>
#include <TTree.h>

// TRTFramework / NTupler
#include <TRTFramework/Algorithm.h>
#include <MCDNTupler/NTuplerBase.h>

// C++
#include <sstream>

ClassImp(xTRT::NTuplerBase)

xTRT::NTuplerBase::NTuplerBase() {}

xTRT::NTuplerBase::~NTuplerBase() {}

void xTRT::NTuplerBase::clearHits() {
 // std::cout << "clearHits() start. m_L length: " << m_L.size() << std::endl << "Clearing the vectors" << std::endl;
  m_nTRThitsMan = 0;
  m_nPrechitsMan = 0;
  m_HTMB.clear();
  m_bitPattern.clear();
  m_gasType.clear();
  m_bec.clear();
  m_layer.clear();
  m_strawlayer.clear();
  m_strawnumber.clear();
  m_drifttime.clear();
  m_tot.clear();
  m_T0.clear();
  m_type.clear();
  m_localTheta.clear();
  m_localPhi.clear();
  m_HitZ.clear();
  m_HitR.clear();
  m_rTrkWire.clear();
  m_ZR.clear();
  m_L.clear();
  m_isPrec.clear();

  //std::cout << "clearHits() start. m_L length: " << m_L.size() << std::endl << std::endl;
}

bool xTRT::NTuplerBase::fillHitBasedVariables(const xAOD::TrackParticle* track,
                                              const xTRT::MSOS* msos,
                                              const xTRT::DriftCircle* driftCircle,
                                              const bool type0only,
                                              const bool isMC) {
  auto hit = xTRT::getHitSummary(track,msos,driftCircle,isMC);
  if ( hit.tot < 0 || hit.L < 0 ) return false;
  if ( type0only && (hit.type != 0) ) return false;
  if ( hit.HTMB ) m_nHThitsMan++;

  if ( hit.tot > 0.005 && hit.L > 0.00005 ) {
    m_sumL += hit.L;
    m_sumToT += hit.tot;
  }

  m_type.push_back(hit.type);
  m_bitPattern.push_back(hit.bitPattern);
  m_HTMB.push_back(hit.HTMB);
  m_gasType.push_back(hit.gasType);
  m_bec.push_back(hit.bec);
  m_layer.push_back(hit.layer);
  m_strawlayer.push_back(hit.strawlayer);
  m_strawnumber.push_back(hit.strawnumber);
  m_drifttime.push_back(hit.drifttime);
  m_tot.push_back(hit.tot);
  m_T0.push_back(hit.T0);
  m_localTheta.push_back(hit.localTheta);
  m_localPhi.push_back(hit.localPhi);
  m_HitZ.push_back(hit.HitZ);
  m_HitR.push_back(hit.HitR);
  m_rTrkWire.push_back(hit.rTrkWire);
  m_ZR.push_back(hit.ZR);
  m_L.push_back(hit.L);
  m_isPrec.push_back(hit.isPrec);

  if ( hit.isPrec ) m_nPrechitsMan++;
  m_nTRThitsMan++;

  return true;
}
