#ifndef MCDNTupler_NTuplerAlg_h
#define MCDNTupler_NTuplerAlg_h

// TRTFramework
#include <TRTFramework/TNPAlgorithm.h>
#include <MCDNTupler/NTuplerBase.h>

namespace xTRT {

  class NTuplerAlg : public xTRT::TNPAlgorithm, public xTRT::NTuplerBase {

  private:

    bool m_type0only;       //!

    TTree* m_tree_el_tags;   //!
    TTree* m_tree_el_probes; //!
    TTree* m_tree_mu;        //!

    TTree* m_tree_el_mc; //!
    TTree* m_tree_mu_mc; //!

    TTree* m_tree_invM_el;   //!
    TTree* m_tree_invM_mu;   //!

    TTree* m_tree_tracks; //!

    float m_invM_el; //!
    float m_invM_mu; //!

    int m_IDTSCut; //!

  public:

    NTuplerAlg();
    virtual ~NTuplerAlg();

    virtual EL::StatusCode histInitialize() final;
    virtual EL::StatusCode initialize()     final;
    virtual EL::StatusCode execute()        final;

    void analyzeTrack(const xAOD::TrackParticle* track, TTree *tree2fill);

    ClassDefOverride(xTRT::NTuplerAlg, 1);

  };

}

#endif
