#ifndef MCDNTupler_NTuplerBase_h
#define MCDNTupler_NTuplerBase_h

// TRTFramework
#include <TRTFramework/AtlasIncludes.h>

class TTree;

namespace xTRT {

  class NTuplerBase {

  protected:

    bool m_saveHits = false; //!
    bool m_saveTrackTree = false; //!
    bool m_saveInvTree = false; //!
    bool m_saveElTree = false; //!
    bool m_saveMuTree = false; //!

    int         m_runN;         //!
    long long   m_evtN;         //!
    float       m_avgMu;        //!
    float       m_weight;       //!
    float       m_trkOcc;       //!
    float       m_pT;           //!
    float       m_lep_pT;       //!
    float       m_p;            //!
    float       m_eta;          //!
    float       m_phi;          //!
    float       m_theta;        //!
    float       m_eProbHT;      //!
    int         m_nTRThits;     //!
    int         m_nTRThitsMan;  //!
    int         m_nTRTouts;     //!
    int         m_nTRToutsMan;  //!
    int         m_nHThits;      //!
    int         m_nHThitsMan;   //!
    int         m_nPrechitsMan; //!
    int         m_nArhits;      //!
    int         m_nArhitsMan;   //!
    int         m_nXehits;      //!
    int         m_nXehitsMan;   //!
    float       m_dEdxNoHT;     //!
    int         m_nHitsdEdx;    //!
    float       m_sumL;         //!
    float       m_sumToT;       //!
    float       m_sumToTsumL;   //!
    float       m_fAr;          //!
    float       m_fHTMB;        //!
    float       m_PHF;          //!
    float       m_dEdx;         //!

    std::vector<uint>  m_HTMB;        //!
    std::vector<uint>  m_bitPattern;  //!
    std::vector<uint>  m_gasType;     //!
    std::vector<int>   m_bec;         //!
    std::vector<int>   m_layer;       //!
    std::vector<int>   m_strawlayer;  //!
    std::vector<int>   m_strawnumber; //!
    std::vector<float> m_drifttime;   //!
    std::vector<float> m_tot;         //!
    std::vector<float> m_T0;          //!
    std::vector<int>   m_type;        //!
    std::vector<float> m_localTheta;  //!
    std::vector<float> m_localPhi;    //!
    std::vector<float> m_HitZ;        //!
    std::vector<float> m_HitR;        //!
    std::vector<float> m_rTrkWire;    //!
    std::vector<float> m_L;           //!
    std::vector<float> m_ZR;          //!
    std::vector<int> m_isPrec;      //!

  public:

    NTuplerBase();
    virtual ~NTuplerBase();

    void clearHits();
    bool fillHitBasedVariables(const xAOD::TrackParticle* track,
                               const xTRT::MSOS* msos,
                               const xTRT::DriftCircle* driftCircle,
                               const bool type0only,
                               const bool isMC);

    ClassDef(xTRT::NTuplerBase, 1);

  };

}

#define CONNECT_BRANCHES(TREE)                                           \
  { TREE->Branch("runN",        &m_runN);                                \
    TREE->Branch("avgmu",       &m_avgMu);                               \
    TREE->Branch("weight",      &m_weight);                              \
    TREE->Branch("trkOcc",      &m_trkOcc);                              \
    TREE->Branch("p",           &m_p);                                   \
    TREE->Branch("pT",          &m_pT);                                  \
    TREE->Branch("lep_pT",      &m_lep_pT);                              \
    TREE->Branch("eta",         &m_eta);                                 \
    TREE->Branch("phi",         &m_phi);                                 \
    TREE->Branch("eProbHT",     &m_eProbHT);                             \
    TREE->Branch("nTRThits",    &m_nTRThits);                            \
    TREE->Branch("nTRThitsMan", &m_nTRThitsMan);                         \
    TREE->Branch("nTRTouts",    &m_nTRTouts);                            \
    TREE->Branch("nArhits",     &m_nArhits);                             \
    TREE->Branch("nXehits",     &m_nXehits);                             \
    TREE->Branch("nHThitsMan",  &m_nHThitsMan);                          \
    TREE->Branch("nPrechitsMan",&m_nPrechitsMan);                        \
    TREE->Branch("NhitsdEdx",   &m_nHitsdEdx);                           \
    TREE->Branch("sumToT",      &m_sumToT);                              \
    TREE->Branch("sumL",        &m_sumL);                                \
    TREE->Branch("sumToTsumL",  &m_sumToTsumL);                          \
    TREE->Branch("fAr",         &m_fAr);                                 \
    TREE->Branch("fHTMB",       &m_fHTMB);                               \
    TREE->Branch("PHF",         &m_PHF);                                 \
    TREE->Branch("dEdx",        &m_dEdx);                                \
    if ( m_saveHits ) {                                                        \
      TREE->Branch("hit_bitPattern", &m_bitPattern);  \
      TREE->Branch("hit_HTMB",       &m_HTMB);        \
      TREE->Branch("hit_gasType",    &m_gasType);     \
      TREE->Branch("hit_bec",        &m_bec);         \
      TREE->Branch("hit_layer",      &m_layer);       \
      TREE->Branch("hit_strawlayer", &m_strawlayer);  \
      TREE->Branch("hit_strawnumber",&m_strawnumber); \
      TREE->Branch("hit_drifttime",  &m_drifttime);   \
      TREE->Branch("hit_tot",        &m_tot);         \
      TREE->Branch("hit_T0",         &m_T0);          \
      TREE->Branch("hit_localTheta", &m_localTheta);  \
      TREE->Branch("hit_localPhi",   &m_localPhi);    \
      TREE->Branch("hit_HitZ",       &m_HitZ);        \
      TREE->Branch("hit_HitR",       &m_HitR);        \
      TREE->Branch("hit_rTrkWire",   &m_rTrkWire);    \
      TREE->Branch("hit_L",          &m_L);           \
      TREE->Branch("hit_ZR",         &m_ZR);          \
      TREE->Branch("hit_isPrec",     &m_isPrec);      \
    } }

#endif
